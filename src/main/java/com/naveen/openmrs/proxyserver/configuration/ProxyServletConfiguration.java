package com.naveen.openmrs.proxyserver.configuration;

import org.mitre.dsmiley.httpproxy.ProxyServlet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ProxyServletConfiguration {
    @Autowired
    ProxyConfiguration proxyConf;

    @Bean
    public ServletRegistrationBean servletRegistrationBean() {
        ServletRegistrationBean servletRegBean = new ServletRegistrationBean(new ProxyServlet(), proxyConf.getServletUrl());
        servletRegBean.addInitParameter(ProxyServlet.P_LOG, proxyConf.getEnableLog());
        servletRegBean.addInitParameter("targetUri", proxyConf.getTargetUrl());
        return servletRegBean;
    }
}
