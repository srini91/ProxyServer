package com.naveen.openmrs.proxyserver.configuration;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix = "proxy")
@Component
@Getter
@Setter
public class ProxyConfiguration {
    private String targetUrl;
    private String servletUrl;
    private String enableLog;

}

