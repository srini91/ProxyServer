# How to Use

## ``mvn clean package``
- Generates war file which can be deployed in tomcat container

### If you want to use it for container less 

- Remove provided            
 ` <scope>provided</scope> ` from 
 ~~~~
<dependency>
   <groupId>org.springframework.boot</groupId>
   <artifactId>spring-boot-starter-tomcat</artifactId>
   <scope>provided</scope>
 </dependency>
 ~~~~
  in **pom.xml** and change packaging type to `<packaging>jar</packaging>`
  
 ### Configuration
 
 > Find a file named application.yaml in resource folder
 ```
 proxy:
   targetUrl: http://localhost:8082/proxy
   servletUrl: /*
   enableLog: true
 ```
 > targetUrl - url where it should be redirected
 
 > serverUrl - url mapping
 
 > enableLog - to log the url log info